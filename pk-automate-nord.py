from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import subprocess
import requests
import time

# Define the executable path and parameters
# executable_path = r"C:\Program Files\Cloudflare\Cloudflare WARP\warp-cli.exe"
executable_path = "nordvpn"
parameter_verbose = "-v"
parameter_connect = "connect"
parameter_disconnect = "disconnect"
parameter_server = "sg" #sg:singapore

def connect_proxy():
    # Run the executable with parameters and capture the output
    result = subprocess.run([executable_path, parameter_connect, parameter_server], capture_output=True, text=True)
    # Get the output as a string
    output = result.stdout.strip()
    # Print the output
    # print(output)

def disconnect_proxy():
    # Disconnect proxy
    # Run the executable with parameters and capture the output
    result = subprocess.run([executable_path, parameter_disconnect], capture_output=True, text=True)
    # Get the output as a string
    output = result.stdout.strip()
    # Print the output
    # print(output)


# Set up the Chrome driver with Selenium Wire configuration
options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--incognito")
options.add_argument("--ignore-certificate-errors")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Suppress error messages
#Linux
options.add_argument("disable-infobars")
options.add_argument("--disable-extensions")
#options.add_argument("--disable-gpu") #windows only
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--no-sandbox")
options.add_argument("--remote-debugging-port=9222")

# Configure the Selenium
seleniumwire_options = {
    'suppress_connection_errors': True,
}

last_ip_address = "0.0.0.0"
delay = 3 #in second
print(last_ip_address)
print(f"Delay: {delay} second")

# Repeat the code multiple times
i = 0
c = 0
num_iterations = 1
# while True:
for i in range(num_iterations):
    i += 1
    print(f"Iteration {i}")
    print(f"Count {c}")
    #Start Proxy
    try:
        connect_proxy()
        print("Connect proxy.")
        time.sleep(3)
    except Exception as e:
        print(f"[ERROR] PROXY: {str(e)}")

    # try:
    #     # Test proxy ip rotation
    #     r = requests.get('https://ipv4.webshare.io/', timeout=10)
    #     print(f"IP by Request: {r.text}")
    # except:
    #     pass

    # Set up the Chrome driver
    driver = webdriver.Chrome(
        # ChromeDriverManager().install(),
        "chromedriver", #Linux only
        seleniumwire_options=seleniumwire_options,
        options=options,
    )

    wait = WebDriverWait(driver, 20)

    # Visit a website to check the IP address
    try:
        driver.get('https://ipv4.webshare.io/')
        print(f"Visit URL: {driver.current_url}")
        ip_element = driver.find_element(By.TAG_NAME, "pre")
        # Get the displayed IP address
        ip_address = ip_element.text
        print(f"Own IP address: {ip_address}")
        print(f"Last IP address: {last_ip_address}")
        if (str(ip_address) == str(last_ip_address)):
            print("Duplicate IP")
            # disconnect_proxy()
            # print("Disconnect proxy.")
            # print(f"Delay started: {delay} second")
            # time.sleep(delay)
            # continue
        else:
            last_ip_address = ip_address
    except Exception as e:
        print(f"[Error] VISIT WEB IP: {str(e)}")

    # Set the URL of the page to visit
    url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"
    initial_url = url

    try:
        # Visit the page
        driver.get(url)
        print(f"Visit URL: {url}")
    # except Exception as e:
    #     print(f"[Error] VISIT POLLING URL: {str(e)}")

    # try:
        # Wait for the presence of an element that indicates the page has loaded
        wait.until(EC.presence_of_element_located((By.ID, "btn3")))
        # Find the button element and click on it
        button_element = driver.find_element(By.ID, "btn3")
        print(button_element.text)
    # except Exception as e:
    #     print(f"[ERROR] BUTTON: {str(e)}")

    # try:
        # Find the button element and click on it
        button_element.click()
        print("Button clicked successfully.")
    # except Exception as e:
    #     print(f"[ERROR] CLICK: {str(e)}") 

    # try:
        # Wait for the alert element to be visible
        wait.until(EC.visibility_of_element_located((By.ID, "alert")))
        # Get the text of the alert element
        alert_text = driver.find_element(By.ID, "alert")
        if alert_text.text:
            print(alert_text.text)
    except Exception as e:
        print(f"[EXCEPTION]: {str(e)}")

    if driver.current_url != initial_url:
        print(f"Sukses: {driver.current_url}")
        c += 1

    # Refresh the browser to clear cookies and session
    driver.refresh()
    print("Clear cookies and session.")
    # Quit the driver
    driver.quit()
    print("Close browser.")

    #End Proxy
    try:
        disconnect_proxy()
        print("Disconnect proxy.")
        print(f"Delay started: {delay} second")
        time.sleep(delay)
    except Exception as e:
        print(f"[ERROR] PROXY: {str(e)}")