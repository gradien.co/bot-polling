from seleniumwire import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import undetected_chromedriver as uc 
import requests
import ssl

# ssl._create_default_https_context = ssl._create_unverified_context
 
# Set the proxy configuration
proxy_host = 'p.webshare.io'
proxy_port = '9999'
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'
proxy_url = f"http://{proxy_host}:{proxy_port}"
# proxy_url = f"http://{proxy_username}:{proxy_password}@{proxy_host}:80"

# Create a Proxy object
proxy = Proxy()
proxy.proxy_type = ProxyType.MANUAL
proxy.http_proxy = proxy_url
proxy.ssl_proxy = proxy_url

proxies = {
    "http": proxy_url,
    "https": proxy_url
}

# Create desired capabilities and set the proxy
capabilities = webdriver.DesiredCapabilities.CHROME.copy()
proxy.add_to_capabilities(capabilities)

# Set the IP addresses or CIDR ranges to whitelist
whitelisted_ips = []

# Set up the Chrome driver with Selenium Wire configuration
# options = webdriver.ChromeOptions()
options = uc.ChromeOptions()
# options.add_argument("--headless")
options.add_argument("--incognito")
options.add_argument("--ignore-certificate-errors")
options.add_argument(f"--proxy-server=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
# options.add_argument(f"--proxy-https=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
# options.add_argument("--disable-blink-features=AutomationControlled")
# options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Suppress error messages

# Configure the whitelist IP addresses
seleniumwire_options = {
    'whitelist_ips': whitelisted_ips,
    'suppress_connection_errors': True,
    # 'proxy': {
    #     'http': proxy_url,
    #     'https': proxy_url,
    #     'verify_ssl': False,
    # },
}

# uc.install()
driver = uc.Chrome(
            # ChromeDriverManager().install(),
            # seleniumwire_options=seleniumwire_options,
            options=options,
            # desired_capabilities=capabilities,
            use_subprocess=True
        )

# Test proxy ip rotation
r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
print(f"IP Proxy: {r.text}")

try:
    driver.get('https://ipv4.webshare.io/')
    print(f"Visit URL: {driver.current_url}")
    
    ip_element = driver.find_element(By.TAG_NAME, "pre")

    # Get the displayed IP address
    ip_address = ip_element.text
    print(f"Own IP address: {ip_address}")
except Exception as e:
    print(f"Page navigation failed. Error: {str(e)}")


# Set the URL of the page to visit
# url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"
url = "https://www.datacamp.com/users/sign_in"

driver.get(url) 
 
# time.sleep(20) 
 
driver.save_screenshot("screenshot.png") 
 
driver.close()