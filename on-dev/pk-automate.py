import scrapy
from scrapy.crawler import CrawlerRunner
from twisted.internet import reactor, defer
import subprocess
import webbrowser

# Define the executable path and parameters
executable_path = r"C:\Program Files\Cloudflare\Cloudflare WARP\warp-cli.exe"
parameter_verbose = "-v"
parameter_connect = "connect"
parameter_disconnect = "disconnect"

class PKAutomate(scrapy.Spider):
    name = "pk_automate"
    start_urls = ["https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"]

    
    def start_requests(self):
        # Run Windows command prompt before starting the spider
        # Run the executable with parameters and capture the output
        result = subprocess.run([executable_path, parameter_verbose, parameter_connect], capture_output=True, text=True)
        # Get the output as a string
        output = result.stdout.strip()
        # Print the output
        print(output)
        
        yield scrapy.Request(url=self.start_urls[0], callback=self.parse, meta={'cookiejar': 1})

    def parse(self, response):
        # Klik tombol
        button_xpath = "//button[@id='btn3']"
        yield response.click(button_xpath, callback=self.after_click, meta={'cookiejar': response.meta['cookiejar']})

    def after_click(self, response):
        # Mengambil data respons
        result = {
            "message": response.css("#alert::text").extract(),
            "response": response.css("h2::text").extract(),
        }
        print(result)
        
    def disconnect_proxy(self, response):
        # Disconnect proxy
        # Run the executable with parameters and capture the output
        result = subprocess.run([executable_path, parameter_verbose, parameter_disconnect], capture_output=True, text=True)
        # Get the output as a string
        output = result.stdout.strip()
        # Print the output
        print(output)

        # Menghapus cookies atau menyegarkan browser
        # self.clear_cookies()
        # Atau gunakan baris berikut untuk menyegarkan browser menggunakan modul webbrowser
        webbrowser.refresh()

    
       

# Menjalankan crawler sebanyak beberapa kali
num_iterations = 5  # Ubah sesuai dengan jumlah iterasi yang diinginkan

@defer.inlineCallbacks
def run_crawler():
    runner = CrawlerRunner(settings={
        "USER_AGENT": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.182 Safari/537.36",
        "ROBOTSTXT_OBEY": True,
        "DOWNLOAD_DELAY": 3,
        "COOKIES_ENABLED": True,
        "REQUEST_FINGERPRINTER_IMPLEMENTATION": "2.7",
    
    })

    for i in range(num_iterations):
        print(f"Iterasi {i+1}")
        yield runner.crawl(PKAutomate)

    reactor.stop()

run_crawler()
reactor.run()
