from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import requests
import msvcrt


# Set the proxy configuration
proxy_host = 'p.webshare.io'
proxy_port = '80'
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'

proxies = {
    "http": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/",
    "https": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/"
}

# Set the IP addresses or CIDR ranges to whitelist
whitelisted_ips = []

# Set up the Chrome driver with Selenium Wire configuration
options = webdriver.ChromeOptions()
options.add_argument("--ignore-certificate-errors")
options.add_argument(f"--proxy-server=http://{proxy_host}:{proxy_port}")
options.add_argument(f"--proxy-auth={proxy_username}:{proxy_password}")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_argument("--incognito")


# Configure the whitelist IP addresses
seleniumwire_options = {
    'whitelist_ips': whitelisted_ips
}

# Repeat the code multiple times until 'S' key is pressed
i = 0
while True:
    print(f"Iteration {i+1}")
    try:
        # Test proxy ip rotation
        r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
        print(r.text)

        # Initialize the Chrome driver
        driver = webdriver.Chrome(ChromeDriverManager().install(
        ), seleniumwire_options=seleniumwire_options, options=options)

        # Set the URL of the page to visit
        url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"

        # Visit the page
        driver.get(url)

        # Wait for the presence of an element that indicates the page has loaded
        wait = WebDriverWait(driver, 10)
        wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#btn3")))

        # Find the button element and click on it
        button_element = driver.find_element(By.CSS_SELECTOR, "#btn3")
        button_element.click()

        # Wait for the alert element to be visible
        wait.until(EC.visibility_of_element_located((By.ID, "alert")))

        # Get the text of the alert element
        alert_text = driver.find_element(By.ID, "alert").text
        print(alert_text)

        # Refresh the browser to clear cookies and session
        driver.refresh()

        # Quit the driver
        driver.quit()

    except:
        print('failed')
        pass

    i += 1

    # Check for keypress event
    if msvcrt.kbhit():
        key = msvcrt.getch().decode()
        if key.lower() == 's':
            break

