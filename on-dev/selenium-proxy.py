from seleniumwire import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import requests

# Set the proxy configuration
proxy_host = 'p.webshare.io'
proxy_port = '9999'
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'
seleniumwire_options = {
    'proxy': {
        'http': f'http://p.webshare.io:9999',
        'verify_ssl': False,
    },
}

proxies = {
    "http": f"http://{proxy_host}:{proxy_port}/",
    "https": f"http://{proxy_host}:{proxy_port}/"
}

# Create a Proxy object
proxy = Proxy()
proxy.proxy_type = ProxyType.MANUAL
proxy.http_proxy = f'http://{proxy_host}:{proxy_port}'
proxy.ssl_proxy = f'http://{proxy_host}:{proxy_port}'

# Create desired capabilities and set the proxy
capabilities = webdriver.DesiredCapabilities.CHROME.copy()
proxy.add_to_capabilities(capabilities)

# Set up the Chrome driver with Selenium Wire configuration
options = webdriver.ChromeOptions()
options.add_argument("--headless")

 # Test proxy ip rotation
r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
print(f"IP Proxy: {r.text}")

driver = webdriver.Chrome(
    seleniumwire_options=seleniumwire_options,
    options=options,
    desired_capabilities=capabilities
)
driver.get('https://ipv4.webshare.io/')

ip_element = driver.find_element(By.TAG_NAME, "pre")
# Get the displayed IP address
ip_address = ip_element.text
print(f"Own IP address: {ip_address}")